package testcases;

import org.junit.Ignore;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.SeleniumBase;

public class MergeLead extends Annotation {
	
	@BeforeTest(groups="regression")
	public void setData() {
		testcaseName= "MergeLead";
		testcaseDec = "Merge the Lead in leaftaps";
		author      = "Indu";
		category    = "Smoke";
	}
	
	//@Ignore
	//@Test(enabled=false)
	@Test(groups = "regression")
	public void login() {
		
		WebElement elecrmsfa = locateElement("link", "CRM/SFA");
		click(elecrmsfa);
		WebElement eleleads = locateElement("link", "Leads");
		click(eleleads);
		WebElement elemerge = locateElement("link", "Merge Leads");
		click(elemerge);
		WebElement elenewwindow = locateElement("xpath", "(//img[@alt='Lookup'])[1]");
		click(elenewwindow);
		switchToWindow(1);
		WebElement eleleadid = locateElement("xpath", "(//input[@id='ext-gen26'])[1]");
		clearAndType(eleleadid, "10150");
		WebElement elefindleads = locateElement("link", "Find Leads");
		click(elefindleads);
		switchToWindow(1);
		
		
		
		/*WebElement elemerge = locateElement("link", "Merge Leads");
		click(elemerge);
		WebElement elenewwindow = locateElement("xpath", "(//img[@alt='Lookup'])[1]");
		click(elenewwindow);
		switchToWindow(1);
		WebElement eleleadid = locateElement("xpath", "(//input[@id='ext-gen26'])[1]");
		clearAndType(eleleadid, "10055");
		WebElement elefindlead = locateElement("id", "ext-gen114");   //working path
		WebElement elefindlead = locateElement("xpath", "//button[@id='ext-gen114']");   //working path
		click(elefindlead);
		WebElement eleclicklead = locateElement("id", "ext-gen290");
		click(eleclicklead);
		switchToWindow(0);
		WebElement elenewwindow1 = locateElement("xpath", "(//img[@alt='Lookup'])[0]");
		click(elenewwindow1);
		switchToWindow(2);
		WebElement eleleadid1 = locateElement("xpath", "(//input[@id='ext-gen26'])[1]");
		clearAndType(eleleadid1, "10057");
		WebElement elefindlead1 = locateElement("id", "ext-gen114");
		click(elefindlead1);
		WebElement eleclicklead1 = locateElement("id", "ext-gen290");
		click(eleclicklead1);
		switchToWindow(0);*/
	
		
}
}
