package testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
public class DuplicateLead extends Annotation{
	
	@BeforeClass
	public void setData() {
		testcaseName= "DuplicateLead";
		testcaseDec = "DuplicateLead the Lead in leaftaps";
		author      = "Indu";
		category    = "Smoke";
	}
		@Test
		public void duplicateLead() throws InterruptedException {
			click(locateElement("link", "CRM/SFA"));
			click(locateElement("link", "Leads"));
			click(locateElement("link", "Find Leads"));
			click(locateElement("xpath","//span[text()='Phone']"));
			clearAndType(locateElement("name", "phoneNumber"), "100"); 
		    click(locateElement("xpath","//button[text()='Find Leads']"));
		    Thread.sleep(1000);
		    String text = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").getText();
		    click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		    click(locateElement("link", "Duplicate Lead"));
		    click(locateElement("name", "submitButton")); 

		}

}
