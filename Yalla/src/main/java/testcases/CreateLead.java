package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.yalla.selenium.api.base.SeleniumBase;

import java.io.IOException;

public class CreateLead extends Annotation{
	
	@BeforeTest(groups="smoke")
	public void setData() {
		testcaseName= "CreateLead";
		testcaseDec = "Create a new Lead in leaftaps";
		author      = "Indu";
		category    = "Smoke";
		excelFileName = "CreateLead";
	}
	
	//@Test(invocationCount = 2, invocationTimeOut = 10000)
	//@Test(priority =1)
	//@Test(groups = "smoke")
	public void login1() {
		
		WebElement elecrmsfa = locateElement("link", "CRM/SFA");
		click(elecrmsfa);
		WebElement elecreatelead = locateElement("link", "Create Lead");
		click(elecreatelead);
		WebElement elecompany = locateElement("id", "createLeadForm_companyName");
		clearAndType(elecompany, "IBM");
		WebElement elefirstname = locateElement("id", "createLeadForm_firstName");
		clearAndType(elefirstname, "Indu");
		WebElement elelastname = locateElement("id", "createLeadForm_lastName");
		clearAndType(elelastname, "Hari");
		WebElement elesource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingIndex(elesource, 2);
		WebElement elecreateleadclick = locateElement("name","submitButton");
		click(elecreateleadclick);
}
	
	
	//@Test(priority=-1)
	@Test(groups ="smoke", dataProvider="fetchData")
	
public void login2(String cname, String fname, String lname) {

		
		WebElement elecrmsfa = locateElement("link", "CRM/SFA");
		click(elecrmsfa);
		WebElement elecreatelead = locateElement("link", "Create Lead");
		click(elecreatelead);
		WebElement elecompany = locateElement("id", "createLeadForm_companyName");
		clearAndType(elecompany, cname);
		WebElement elefirstname = locateElement("id", "createLeadForm_firstName");
		clearAndType(elefirstname, fname);
		WebElement elelastname = locateElement("id", "createLeadForm_lastName");
		clearAndType(elelastname, lname);
		WebElement elesource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingIndex(elesource, 2);
		WebElement elecreateleadclick = locateElement("name","submitButton");
		click(elecreateleadclick);
}
	

	/*@DataProvider(name="createData")

public Object[][] fetchdata() {
		
	
	Object [][] data=new Object [2][3];
	
	data[0][0]= "CTS";
	data[0][1]= "Geetha";
	data[0][2]= "Hari";
	data[1][0]= "TCS";
	data[1][1]= "Nani";
	data[1][2]= "Adhi";
	return data;
	
	
}*/
}



